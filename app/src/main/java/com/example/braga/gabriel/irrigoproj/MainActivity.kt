package com.example.braga.gabriel.irrigoproj

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import java.io.OutputStream
import java.util.*
import android.os.Build
import android.support.v4.app.FragmentActivity
import android.util.Log
import java.io.IOException




class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val REQUEST_ENABLE_BT = 1
    private val TAG = "bluetooth1"

    private val btAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var btSocket: BluetoothSocket? = null
    private var outStream: OutputStream? = null
    private val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private var address = "00:15:FF:F2:19:5F"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    @Throws(IOException::class)
    private fun createBluetoothSocket(device: BluetoothDevice): BluetoothSocket {
        if (Build.VERSION.SDK_INT >= 10) {
            try {
                val m = device.javaClass.getMethod("createInsecureRfcommSocketToServiceRecord", *arrayOf<Class<*>>(UUID::class.java))
                return m.invoke(device, MY_UUID) as BluetoothSocket
            } catch (e: Exception) {

            }

        }
        return device.createRfcommSocketToServiceRecord(MY_UUID)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.get_database -> {
                val dialog = ProgressDialog.show(this@MainActivity, "",
                        "Loading. Please wait...", true)
                val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    //if (!mBluetoothAdapter.isEnabled) {
                        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                        val pairedDevices = mBluetoothAdapter.getBondedDevices()
                        if (pairedDevices.size > 0) {
                            for (device in pairedDevices) {
                                if (device.getName() == "Gabriel Braga"){
                                    address = device.address
                                    sendData("Ola")
                                }
                                    val simpleAlert = AlertDialog.Builder(this@MainActivity).create()
                                    simpleAlert.setTitle("Achei")
                                    simpleAlert.setMessage(device.getName() )
                                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
                                        dialogInterface, i ->

                                    })
                                    simpleAlert.show()
                               // }
                            }
                       // }
                    }

                dialog.dismiss();
            }
            R.id.send_database -> {
                val dialog = ProgressDialog.show(this@MainActivity, "",
                        "Loading. Please wait...", true)

            }
            R.id.search_button -> {

            }
            R.id.bluetooth_conf -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    public override fun onResume() {
        super.onResume()
        val device = btAdapter.getRemoteDevice(address)
        try {
            btSocket = createBluetoothSocket(device)
        } catch (e1: IOException) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e1.message + ".")
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery()

        // Establish the connection.  This will block until it connects.
        try {
            btSocket?.connect()
        } catch (e: IOException) {
            try {
                btSocket?.close()
            } catch (e2: IOException) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.message + ".")
            }

        }
        try {
            outStream = btSocket?.getOutputStream()
        } catch (e: IOException) {
            errorExit("Fatal Error", "In onResume() and output stream creation failed:" + e.message + ".")
        }

    }

    public override fun onPause() {
        super.onPause()



        if (outStream != null) {
            try {
                outStream?.flush()
            } catch (e: IOException) {
                errorExit("Fatal Error", "In onPause() and failed to flush output stream: " + e.message + ".")
            }

        }

        try {
            btSocket?.close()
        } catch (e2: IOException) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.message + ".")
        }

    }

    private fun checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if (btAdapter == null) {
            errorExit("Fatal Error", "Bluetooth not support")
        } else {
            if (btAdapter.isEnabled) {

            } else {
                //Prompt user to turn on Bluetooth
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, 1)
            }
        }
    }

    private fun errorExit(title: String, message: String) {
        Toast.makeText(baseContext, title + " - " + message, Toast.LENGTH_LONG).show()
        finish()
    }

    private fun sendData(message: String) {
        val msgBuffer = message.toByteArray()
        val simpleAlert = AlertDialog.Builder(this@MainActivity).create()
        simpleAlert.setTitle("Achei")
        simpleAlert.setMessage(""+ address + " >>> " + msgBuffer)
        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->

        })
        simpleAlert.show()
        try {
            outStream?.write(msgBuffer)
        } catch (e: IOException) {
            var msg = "In onResume() and an exception occurred during write: " + e.message
            if (address.equals("00:00:00:00:00:00"))
                msg = "$msg.\n\nUpdate your server address from 00:00:00:00:00:00 to the correct address on line 35 in the java code"
            msg = msg + ".\n\nCheck that the SPP UUID: " + MY_UUID.toString() + " exists on server.\n\n"

            errorExit("Fatal Error", msg)
        }

    }
}
